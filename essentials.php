<?php

/**
 * essentials.php - Load, include and instance essential classes
 * Created 26/08/2010
 * @author euTobias
 */

require CORE.'Utils.php';

Utils::addToIncludePath(CORE);
Utils::addToIncludePath(MODULES);

function __autoload($class_name) {
    require_once $class_name.'.php';
}

?>

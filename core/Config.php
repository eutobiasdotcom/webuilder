<?php

/**
 * Config Class
 * Created 26/08/2010
 * @author euTobias
 */

class Config
{
    /**
     * Store all defined configuration
     * @var <Array>
     */
    private $config = array();
    private $instance;

    private function __construct()
    {
        echo 'DUHHHH, it´s a singleton, it will never run :P';
    }

    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $class = __CLASS__;
            self::$instance = new $class;
        }

        return self::$instance;
    }

    public static function write($index,$value)
    {
        $_this = Config::getInstance();
        $_this->config[$index] = $value;
    }

    public static function read($index)
    {
        $_this = Config::getInstance();
        return $this->config[$index];
    }
}

?>

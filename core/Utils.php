<?php 

/**
 * Utils
 * Created 26/08/2010
 * @author euTobias
 */

class Utils
{
    /**
     * Add a directory to the include path of PHP
     * @param <String> $path
     */
    public static function addToIncludePath($path)
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
    }

    public static function debug($var, $return = false)
    {
        if ( Config::read('debugMode') )
        {
            $output  = '';
            $output .= '<pre class="debug">';
            $output .= '<strong>Debug: '.__FILE__.' - On line: '.__LINE__.'</strong>'."\n\n";
            $output .= print_r($var, true);
            $output .= '</pre>';

            if ($return)
                return $output;

            print $output;
        }
    }

}

?>
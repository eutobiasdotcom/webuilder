<?php
/**
 * Index
 * Created 26/08/2010
 * @author euTobias
 */

/*
 * Directory separator
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * Site root
 */
define('ROOT', dirname(__FILE__).DS);

/**
 * Core dir path
 */
define('CORE', ROOT.'core'.DS);

/**
 * Modules dir path
 */
define('MODULES', ROOT.'modules'.DS);

// ################### CODE BELOW

require "essentials.php";

new NavigationManager();

?>

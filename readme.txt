Webuilder análise

#####################

Core Modules

Admin user
--login
--crud
--access level

Channels (Channel hasMany Pages)
-channel crud
-page crud

Contact
-list messages
-reply message
-remove message(s)

News (News hasMany Category)
-category crud
-news crud

#####################

Classes e estrutura de diretórios

core/
  lib/
    Dbo.php
    Dbo.mysql.php
    Dbo.sqlite.php    
  Controller.php
  Model.php
  NavigationManager.php
  View.php
  Utils.php
modules/
  admin_user/
    AdminUserController.php
    AdminUserModel.php
    views/
      login.php
      create.php
      edit.php
      view.php
      delete.php
      access_level.php
  channels/
    ChannelController.php
    ChannelModel.php
    views/
      create.php
      edit.php
      view.php
      delete.php
  pages/
    PageController.php
    PageModel.php
    views/
      create.php
      edit.php
      view.php
      delete.php
  contact/
    ContactController.php
    ContactModel.php
    views/
      list.php
      reply.php
      remove.php
  news/
    NewsController.php
    NewsModel.php
    views/
      create.php
      edit.php
      view.php
      delete.php
  categories/
    CategoryController.php
    CategoryModel.php
    views/
      create.php
      edit.php
      view.php
      delete.php
index.php